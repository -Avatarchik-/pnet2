﻿extern alias pnet;
using System;
using System.Linq;
using pnet::JetBrains.Annotations;
using PNetC;
using UnityEngine;
using PNet;
using System.Collections;
using System.Reflection;
using Debug = UnityEngine.Debug;
using NetworkStateSynchronization = UnityEngine.NetworkStateSynchronization;

namespace PNetU
{
    /// <summary>
    /// network synchronization
    /// </summary>
    [AddComponentMenu("PNet/Network View")]
    public partial class NetworkView : MonoBehaviour
    {
        private PNetC.NetworkView _networkView;
        internal PNetC.NetworkView NetView { get { return _networkView; } }
        internal void SetNetworkView(PNetC.NetworkView netView)
        {
            _networkView = netView;
            IsMine = netView.IsMine;
            OwnerId = netView.OwnerId;
            ViewId = netView.Id;

            netView.StateSynchronization = _stateSynchronization.ToPNet();

            _networkView.ReceivedStream += StreamDeserializeCaller;
            _networkView.Destroyed += OnNetDestroyed;
            _networkView.Hidden += OnNetHidden;

            if (_queuedSer != null)
            {
                _networkView.SetStreamSerialization(_queuedSer);
                _networkView.StreamSize = _queuedStreamSize;
            }

            var components = gameObject.GetComponents<MonoBehaviour>();

            foreach (var component in components)
            {
                if (component == null)
                {
                    Debug.LogWarning("null monobehaviour? ", this);
                    continue;
                }
                byte id;
                if (component.GetType().GetNetId(out id))
                    SubscribeMarkedRpcsOnComponent(component);
            }
        }

        /// <summary>
        /// Send an rpc
        /// </summary>
        /// <param name="rpcID"></param>
        /// <param name="mode"></param>
        /// <param name="args"></param>
        public void Rpc<T>(byte rpcID, RPCMode mode, params object[] args) where T : class
        {
            _networkView.Rpc<T>(rpcID, mode.ToPNet(), args);
        }

        /// <summary>
        /// Send an rpc to the owner of this object
        /// </summary>
        /// <param name="rpcID"></param>
        /// <param name="args"></param>
        public void RpcToOwner<T>(byte rpcID, params object[] args) where T : class
        {
            _networkView.Rpc<T>(rpcID, RpcMode.OwnerOrdered, args);
        }

        #region serialization
        void Start()
        {
            if (_stateSynchronization == NetworkStateSynchronization.Off || _isSerializing) return;
            
            _isSerializing = true;
            StartCoroutine(Serialize());
        }
        /// <summary>
        /// stream size. Helps prevent array resizing
        /// </summary>
        public int StreamSize { get { return _networkView.StreamSize; } set { _networkView.StreamSize = value; } }
        /// <summary>
        /// set the method to be used during stream serialization
        /// </summary>
        /// <param name="newMethod"></param>
        /// <param name="streamSize"></param>
        public void SetSerializationMethod(Action<NetMessage> newMethod, int streamSize = 16)
        {
            if (newMethod == null) return;
            if (_networkView == null)
            {
                _queuedSer = newMethod;
                _queuedStreamSize = streamSize;
                return;
            }

            _networkView.SetStreamSerialization(newMethod);
            StreamSize = streamSize;
        }

        private Action<NetMessage> _queuedSer;
        private int _queuedStreamSize;


        private NetworkStateSynchronization _stateSynchronization = NetworkStateSynchronization.Off;
        private bool _isSerializing = false;
        /// <summary>
        /// method of serialization
        /// </summary>
        public NetworkStateSynchronization StateSynchronization
        {
            get
            {
                return _stateSynchronization;
            }
            set
            {
                _stateSynchronization = value;

                if (_networkView != null)
                    _networkView.StateSynchronization = _stateSynchronization.ToPNet();

                if (_stateSynchronization != NetworkStateSynchronization.Off && !_isSerializing)
                {
                    _isSerializing = true;
                    StartCoroutine(Serialize());
                }
                else if (_stateSynchronization == NetworkStateSynchronization.Off && _isSerializing)
                {
                    _isSerializing = false;
                }
            }
        }

        [UsedImplicitly]
        void OnEnable()
        {
            //the behaviour has become active again. Check if we have serialization running, and if not, start it up again.
            //otherwise a disable/enable of the behaviour/gameobject will kill the serialization.

            if (_stateSynchronization != NetworkStateSynchronization.Off && !_isSerializing)
            {
                _isSerializing = true;
                StartCoroutine(Serialize());
            }
        }

        private void StreamDeserializeCaller(NetMessage msg)
        {
            if (OnDeserializeStream != null)
            {
                OnDeserializeStream(msg);
            }
        }


        /// <summary>
        /// subscribe to this in order to deserialize streaming data
        /// </summary>
        public Action<NetMessage> OnDeserializeStream = delegate { };

        IEnumerator Serialize()
        {
            while (_isSerializing)
            {
                if (SerializationTime < 0.01f)
                    SerializationTime = 0.01f;

                if (this.enabled)
                {
                    _networkView.SerializeStream();
                }
                yield return new WaitForSeconds(SerializationTime);
            }
        }

        /// <summary>
        /// Time between each stream send serialization
        /// </summary>
        public float SerializationTime = 0.05f;

        #endregion

        #region rpc subscription
        /// <summary>
        /// Subscribe all the marked rpcs on the supplied component
        /// </summary>
        /// <param name="behaviour"></param>
        public void SubscribeMarkedRpcsOnComponent(MonoBehaviour behaviour)
        {
            _networkView.SubscribeMarkedRpcsOnComponent(behaviour);
        }

        /// <summary>
        /// Subscribe to an rpc
        /// </summary>
        /// <param name="componentId"></param>
        /// <param name="rpcID">id of the rpc</param>
        /// <param name="rpcProcessor">action to process the rpc with</param>
        /// <param name="overwriteExisting">overwrite the existing processor if one exists.</param>
        /// <returns>Whether or not the rpc was subscribed to. Will return false if an existing rpc was attempted to be subscribed to, and overwriteexisting was set to false</returns>
        public bool SubscribeToRpc(byte componentId, byte rpcID, Action<NetMessage> rpcProcessor, bool overwriteExisting = true)
        {
            return _networkView.SubscribeToRpc(componentId, rpcID, rpcProcessor, overwriteExisting);
        }

        public bool SubscribeToFunc(byte componentId, byte rpcId, Func<NetMessage, object> func,
            bool overwriteExisting = true)
        {
            return _networkView.SubscribeToFunc(componentId, rpcId, func, overwriteExisting);
        }

        /// <summary>
        /// Unsubscribe from an rpc
        /// </summary>
        /// <param name="componentId"></param>
        /// <param name="rpcID"></param>
        public void UnsubscribeFromRpc(byte componentId, byte rpcID)
        {
            _networkView.UnsubscribeFromRpc(componentId, rpcID);
        }

        public void UnsubscribeFromRpcs(byte componentId)
        {
            _networkView.UnsubscribeFromRpcs(componentId);
        }
        #endregion

        #region proxy
        /// <summary>
        /// Add a proxy to the network view to use for Proxy`T()
        /// </summary>
        /// <param name="proxy"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public void AddProxy(INetComponentProxy proxy)
        {
            _networkView.AddProxy(proxy);
        }

        public void RemoveProxy(INetComponentProxy proxy)
        {
            _networkView.RemoveProxy(proxy);
        }

        public void RemoveProxy<T>()
        {
            _networkView.RemoveProxy<T>();
        }

        /// <summary>
        /// the value set from Proxy(INetComponentProxy proxy)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Proxy<T>(RpcMode mode)
        {
            return _networkView.Proxy<T>(mode);
        }

        /// <summary>
        /// the value set from Proxy(IRoomProxy proxy)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Proxy<T>()
        {
            return _networkView.Proxy<T>();
        }

        #endregion

        /// <summary>
        /// Subscribe to this to know when an object is being destroyed by the server.
        /// </summary>
        public event Action<byte> OnRemove;
        /// <summary>
        /// Similar to OnRemove, but being hidden.
        /// </summary>
        public event Action OnHidden;
        /// <summary>
        /// run once we've finished setting up the networkview variables
        /// </summary>
        public event Action OnFinishedCreation;

        /// <summary>
        /// Whether or not to destroy the gameobject this is attached to when destroying the networkview
        /// </summary>
        public bool DestroyGameObjectOnNetworkDestroy = true;
        
        /// <summary>
        /// Whether or not to destroy the gameobject this is attached to when hiding the networkview
        /// </summary>
        public bool DestroyGameObjectOnHidden = true;

        [UsedImplicitly]
        private void OnDestroy()
        {
            OnRemove = null;
            OnHidden = null;
            OnFinishedCreation = null;
            OnDeserializeStream = null;

            CleanupNetView();
        }

        #region NetworkViewID
        /// <summary>
        /// If i'm the owner
        /// </summary>
        public bool IsMine { get; internal set; }
        /// <summary>
        /// identifier for the network view
        /// </summary>
        public NetworkViewId ViewId = NetworkViewId.Zero;

        /// <summary>
        /// ID of the owner. 0 is the server.
        /// </summary>
        public ushort OwnerId { get; internal set; }

        #endregion

        internal void DoOnFinishedCreation()
        {
            if (OnFinishedCreation != null) OnFinishedCreation();
            OnFinishedCreation = null;
        }

        internal void OnNetDestroyed(byte reasonCode)
        {
            CleanupNetView();

            if (DestroyGameObjectOnNetworkDestroy)
            {
                Debug.Log("Network Destruction. Destroying networkview and gameobject", this);
                Destroy(gameObject);
            }
            else
            {
                Debug.Log("Network destruction. Only destroying networkview", this);
                Destroy(this);
            }

            if (OnRemove == null) return;

            try
            {
                OnRemove(reasonCode);
            }
            catch (Exception e)
            {
                Debug.LogError(e, this);
            }
            finally
            {
                OnRemove = null;
                OnHidden = null;
            }
        }

        internal void OnNetHidden()
        {
            CleanupNetView();

            if (DestroyGameObjectOnHidden)
            {
                Destroy(gameObject);
            }
            else
                Destroy(this);

            if (OnHidden == null) return;

            try
            {
                OnHidden();
            }
            catch (Exception e)
            {
                Debug.LogError(e, this);
            }
            finally
            {
                OnHidden = null;
                OnRemove = null;
            }
        }

        void CleanupNetView()
        {
            if (_networkView != null) //this will get run usually if we're switching scenes
            {
                if (EngineHook.ValidInstance)
                    EngineHook.Instance.Manager.Remove(_networkView);

                _networkView.ReceivedStream -= StreamDeserializeCaller;
                _networkView.Destroyed -= OnNetDestroyed;
                _networkView.Hidden -= OnNetHidden;
                _networkView.SetStreamSerialization(null);
                _networkView.ClearProxies();
                _networkView.ClearSubscriptions();
                _networkView = null;
            }
        }
    }
}
