﻿using System;
using PNet;

namespace PNetC
{
    public partial class Client
    {
        public NetworkConfiguration Configuration { get; private set; }

        public readonly SerializationManager Serializer = new SerializationManager();

        public event Action<string> BeginRoomSwitch;
        public event Action<string> OnFailedToConnect;
        public event Action OnConnectedToServer;
        public event Action OnDisconnectedFromServer;
        public event Action<string> OnFailedRoomSwitch;
        public event Action OnFinishedRoomSwitch;
        /// <summary>
        /// this is only fired for errors/timeouts, not for room switching
        /// </summary>
        public event Action<string> OnDisconnectedFromRoom;

        public Server Server { get; private set; }
        public Room Room { get; private set; }
        /// <summary>
        /// the object to serialize to the server upon connecting, to use for connection approval.
        /// </summary>
        public object HailObject { get; set; }

        /// <summary>
        /// Player id. 0 is unset.
        /// </summary>
        public ushort PlayerId { get; internal set; }

        public NetworkViewManager NetworkManager { get; private set; }
        public SceneViewManager SceneViewManager { get; private set; }
        /// <summary>
        /// Current network time
        /// </summary>
        public double Time { get; set; }

        private readonly ARoomClient _roomClient;
        private readonly ADispatchClient _dispatchClient;

        public Client(ARoomClient roomClient, ADispatchClient dispatchClient)
        {
            _roomClient = roomClient;
            _roomClient.Client = this;

            _dispatchClient = dispatchClient;
            _dispatchClient.Client = this;

            Server = new Server(this);
            Room = new Room(this);

            NetworkManager = new NetworkViewManager(this);
            SceneViewManager = new SceneViewManager(this);
            NetComponentHelper.FindNetComponents();
        }

        /// <summary>
        /// start the server's networking.
        /// </summary>
        /// <param name="configuration"></param>
        public void StartConnection(NetworkConfiguration configuration)
        {
            Configuration = configuration;
            _dispatchClient.Start();
            _roomClient.Start();
        }

        public bool _shuttingDown { get; set; }
        public void Shutdown(string reason = "Shutting down")
        {
            _shuttingDown = true;
            _dispatchClient.Disconnect(reason);
            _roomClient.Disconnect(reason);
        }

        public void ReadQueue()
        {
            _dispatchClient.ReadQueue();
            _roomClient.ReadQueue_Internal();
        }

        internal void OnBeginRoomSwitch(NetMessage msg)
        {
            var address = msg.ReadIPEndPoint();
            var rid = msg.ReadString();
            var token = msg.ReadGuid();
            Debug.Log("Switching to {0} @ {1}", rid, address);
            _roomClient.SwitchRoom(address, rid, token);

            if (Configuration.DeleteNetworkInstantiatesOnDisconnect)
                NetworkManager.DestroyAll();
            if (SceneViewManager.ClearOnRoomSwitch)
                SceneViewManager.ClearSceneIDs();
        }

        public void FinishRoomSwitch()
        {
            if (_roomClient.SwitchState != ARoomClient.waitForSwitchState.WaitForRoomSwitch)
                return;

            _roomClient.WaitForReconnect();
            
            var nmsg = Server.GetMessage(2);
            nmsg.Write(RpcUtils.GetHeader(ReliabilityMode.Ordered, BroadcastMode.Server, MsgType.Internal));
            nmsg.Write(RandPRpcs.FinishedRoomSwitch);
            Server.SendMessage(nmsg, ReliabilityMode.Ordered);
        }

        public void Cleanup()
        {
            HailObject = null;
            BeginRoomSwitch = null;
            OnConnectedToServer = null;
            OnDisconnectedFromServer = null;
            OnDisconnectedFromRoom = null;
            OnFailedToConnect = null;
            OnFailedRoomSwitch = null;
        }

        internal void FinalizeDisconnect()
        {
            _dispatchClient.DisconnectIfStillConnected();
            if (OnDisconnectedFromServer != null)
                OnDisconnectedFromServer();

            if (Configuration.DeleteNetworkInstantiatesOnDisconnect)
            {
                NetworkManager.DestroyAll();
            }
        }

        #region event raisers for _roomClient
        internal void RaiseBeginRoomSwitch(string roomId)
        {
            try
            {
                BeginRoomSwitch.Raise(roomId);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        internal void RaiseFinishedRoomSwitch()
        {
            try
            {
                OnFinishedRoomSwitch.Raise();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        internal void RaiseFailedRoomSwitch(string reason)
        {
            try
            {
                OnFailedRoomSwitch.Raise(reason);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        internal void RaiseDisconnectedFromRoom(string reason)
        {
            try
            {
                OnDisconnectedFromRoom.Raise(reason);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
        #endregion

        #region event raisers for _dispatchClient
        internal void RaiseFailedToConnect(string reason)
        {
            try
            {
                OnFailedToConnect.Raise(reason);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
        internal void RaiseConnectedToServer()
        {
            try
            {
                OnConnectedToServer.Raise();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
        #endregion

        internal void SendSceneViewMessage(NetMessage msg, ReliabilityMode mode)
        {
            _roomClient.InternalSendSceneViewMessage(msg, mode);
        }

        internal void SendViewMessage(NetMessage msg, ReliabilityMode mode)
        {
            _roomClient.InternalSendViewMessage(msg, mode);
        }

        internal void SendRoomStaticMessage(NetMessage msg, ReliabilityMode mode)
        {
            _roomClient.InternalSendStaticMessage(msg, mode);
        }

        internal NetMessage RoomGetMessage(int size)
        {
            return _roomClient.GetMessage(size);
        }

        internal void SendDispatchMessage(NetMessage msg, ReliabilityMode mode)
        {
            _dispatchClient.InternalSendMessage(msg, mode);
        }

        internal NetMessage DispatchGetMessage(int size)
        {
            return _dispatchClient.GetMessage(size);
        }

        internal void TestServerOnlyDisconnect()
        {
#pragma warning disable 618
            _dispatchClient.Disconnect("unequaldisconnect");
#pragma warning restore 618
        }
    }
}
