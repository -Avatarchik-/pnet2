﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using PNet;

namespace PNetC.Impl
{
    public class TcpDispatchClient : ADispatchClient
    {
        private TcpClient _serverClient;
        private Thread _thread;

        protected internal override void Start()
        {
            if (_serverClient != null)
                _serverClient.Close();
            _serverClient = new TcpClient();
            Client.Server.Status = ConnectionStatus.Connecting;
            _serverClient.BeginConnect(Client.Configuration.ServerAddress, Client.Configuration.ServerPort,
                ConnectCallback, _serverClient);
        }

        private void ConnectCallback(IAsyncResult ar)
        {
            var client = ar.AsyncState as TcpClient;
            client.ReceiveBufferSize = 1024;
            client.SendBufferSize = 1024;
            client.NoDelay = true;
            client.LingerState = new LingerOption(true, 10);

            if (client != _serverClient)
            {
                throw new Exception("TcpClient Mismatch");
            }

            try
            {
                client.EndConnect(ar);
            }
            catch (Exception e)
            {
                client.Close();
                RaiseFailedToConnect(e.Message);
                return;
            }

            if (ar.CompletedSynchronously)
            {
                _thread = new Thread(MessageLoop) { IsBackground = true, Name = "TcpDispatchClient MessageLoop" };
                _thread.Start(client);
            }
            else
                MessageLoop(client);
        }

        private void MessageLoop(object state)
        {
            var client = state as TcpClient;
            try
            {
                if (client != _serverClient)
                {
                    throw new Exception("TcpClient Mismatch");
                }

                Client.Server.StatusReason = "Connection established, waiting for auth";
                Debug.Log("Authenticating with server");

                using (var networkStream = client.GetStream())
                {
                    //send auth...
                    if (Client.HailObject != null)
                    {
                        var size = Client.Serializer.SizeOf(Client.HailObject);
                        var msg = GetMessage(size);
                        Client.Serializer.Serialize(Client.HailObject, msg);
                        msg.WriteSize();

                        networkStream.Write(msg.Data, 0, msg.LengthBytes);
                    }

                    var buffer = new byte[1024];
                    NetMessage dataBuffer = null;
                    var bufferSize = 0;
                    var lengthBuffer = new byte[2];
                    var bytesReceived = 0;
                    int readBytes;


                    var authMessages = new Queue<NetMessage>();
                    while (!Client._shuttingDown)
                    {
                        readBytes = networkStream.Read(buffer, 0, buffer.Length);
                        if (readBytes > 0)
                        {
                            var readMessage = NetMessage.GetMessages(buffer, readBytes, ref bytesReceived,
                                ref dataBuffer, ref lengthBuffer, ref bufferSize, authMessages.Enqueue);
                            if (readMessage >= 1)
                                break;
                        }
                        if (!client.Connected)
                            return;
                    }

                    if (authMessages.Count == 0)
                        throw new Exception("Could not read player id");
                    var authMsg = authMessages.Dequeue();

                    var auth = authMsg.ReadBoolean();
                    authMsg.ReadPadBits();
                    if (!auth)
                    {
                        string reason;
                        if (!authMsg.ReadString(out reason))
                            reason = "Not authorized";
                        RaiseFailedToConnect(reason);
                        return;
                    }
                    if (authMsg.RemainingBits < 16)
                        throw new Exception("Could not read player id");
                    var id = authMsg.ReadUInt16();
                    _networkStream = networkStream;
                    ConnectedToServer(id);

                    //and drain the rest of messages that might have come after the auth
                    while (authMessages.Count > 0)
                        EnqueueMessage(authMessages.Dequeue());

                    while (!Client._shuttingDown)
                    {
                        readBytes = networkStream.Read(buffer, 0, buffer.Length);
                        if (readBytes > 0)
                        {
                            NetMessage.GetMessages(buffer, readBytes, ref bytesReceived, ref dataBuffer,
                                ref lengthBuffer, ref bufferSize, EnqueueMessage);
                        }
                        if (!client.Connected)
                            return;
                    }

                }
            }
            catch (ObjectDisposedException ode)
            {
                if (!Client._shuttingDown && ode.ObjectName != "System.Net.Sockets.NetworkStream")
                    Debug.LogException(ode, "{0} disposed when it shouldn't have", ode.ObjectName);
            }
            catch (IOException ioe)
            {
                if (!(ioe.InnerException is SocketException))
                    Debug.LogException(ioe);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                }
                FinalizeDisconnect();
            }
        }

        private void EnsureConnected()
        {
            var msg = GetMessage(2);
            msg.Write(RpcUtils.GetHeader(ReliabilityMode.Ordered, BroadcastMode.Server, MsgType.Internal));
            msg.Write(DandRRpcs.Ping);
            InternalSendMessage(msg, ReliabilityMode.Ordered);
        }

        private readonly Queue<NetMessage> _incomingMessages = new Queue<NetMessage>();
        private NetworkStream _networkStream;

        private void EnqueueMessage(NetMessage msg)
        {
            lock (_incomingMessages)
                _incomingMessages.Enqueue(msg);
        }

        protected internal override void ReadQueue()
        {
            NetMessage[] msgs;
            lock (_incomingMessages)
            {
                msgs = _incomingMessages.ToArray();
                _incomingMessages.Clear();
            }
            foreach (var msg in msgs)
            {
                Client.Server.ConsumeData(msg);
                NetMessage.RecycleMessage(msg);
            }
        }

        protected internal override void Disconnect(string reason)
        {
            var shutReason = GetMessage(reason.Length * 2 + 6);
            shutReason.Write(RpcUtils.GetHeader(ReliabilityMode.Ordered, BroadcastMode.Server, MsgType.Internal));
            shutReason.Write(DandPRpcs.DisconnectMessage);
            shutReason.Write(reason);
            InternalSendMessage(shutReason, ReliabilityMode.Ordered);
            _serverClient.Close();
        }

        protected internal override void DisconnectIfStillConnected()
        {
            _serverClient.Close();
        }

        protected internal override NetMessage GetMessage(int size)
        {
            return NetMessage.GetMessageSizePad(size);
        }

        protected internal override void InternalSendMessage(NetMessage msg, ReliabilityMode mode)
        {
            msg.WriteSize();
            try
            {
                var strm = _networkStream;
                if (strm != null)
                    strm.BeginWrite(msg.Data, 0, msg.LengthBytes, SendCallback, msg);
            }
            catch (ObjectDisposedException ode)
            {
                if (_networkStream != null && ode.ObjectName != "System.Net.Sockets.NetworkStream")
                {
                    Debug.LogException(ode, "{0} disposed when it shouldn't have", ode.ObjectName);
                    _serverClient.Close();
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                _serverClient.Close();
            }
        }

        private void SendCallback(IAsyncResult ar)
        {
            var msg = ar.AsyncState as NetMessage;
            try
            {
                var strm = _networkStream;
                if (strm != null)
                    strm.EndWrite(ar);
            }
            catch (ObjectDisposedException ode)
            {
                if (_networkStream != null && ode.ObjectName != "System.Net.Sockets.NetworkStream")
                {
                    Debug.LogException(ode, "{0} disposed when it shouldn't have", ode.ObjectName);
                    _serverClient.Close();
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                _serverClient.Close();
            }
            finally
            {
                NetMessage.RecycleMessage(msg);
            }
            if (!_serverClient.Connected)
                _serverClient.Close();
        }
    }
}
