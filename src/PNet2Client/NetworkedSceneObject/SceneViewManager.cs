﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PNet;

namespace PNetC
{
    public class SceneViewManager
    {
        private readonly Dictionary<ushort, NetworkedSceneView> _views = new Dictionary<ushort, NetworkedSceneView>();

        public readonly Client Client;
        /// <summary>
        /// For rpcs that are not handled by a subscribed method, either allow or disallow them to continue forwarding.
        /// Default is true.
        /// </summary>
        public bool ConcernedAboutUnhandledRpcs { get; set; }
        /// <summary>
        /// Whether or not to clear the view lookup on room switching. Default is true.
        /// </summary>
        public bool ClearOnRoomSwitch { get; set; }

        internal SceneViewManager(Client client)
        {
            Client = client;
            ConcernedAboutUnhandledRpcs = true;
            ClearOnRoomSwitch = true;
        }

        internal void CallRpc(ushort id, byte rpcID, NetMessage message)
        {
            NetworkedSceneView sceneView;
            if (_views.TryGetValue(id, out sceneView))
            {
                sceneView.CallRpc(rpcID, message);
            }
        }

        internal void Add(NetworkedSceneView view)
        {
            _views[view.NetworkID] = view;
        }

        public void ClearSceneIDs()
        {
            _views.Clear();
        }
    }
}
