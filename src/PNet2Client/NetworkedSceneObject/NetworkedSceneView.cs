﻿using System;
using System.Collections.Generic;
using System.Text;
using PNet;

namespace PNetC
{
    /// <summary>
    /// Objects that exist in a scene with pre-synchronized network id's
    /// </summary>
    public class NetworkedSceneView : IRpcProvider, IProxySingle<ISceneViewProxy>
    {
        private readonly Client _net;

        /// <summary>
        /// The scene/room Network ID of this item. Should be unique per object
        /// </summary>
        public ushort NetworkID { get; set; }

        public SceneViewManager Manager { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="networkID"></param>
        /// <param name="net"></param>
        public NetworkedSceneView(ushort networkID, Client net)
        {
            NetworkID = networkID;
            _net = net;
            Manager = net.SceneViewManager;
            Manager.Add(this);
        }

        /// <summary>
        /// only used for serialization if you really need to. Does not actually make it accessible to the network
        /// </summary>
        /// <param name="networkID"></param>
        public NetworkedSceneView(ushort networkID)
        {
            NetworkID = networkID;
        }

        #region RPC Processing

        readonly Dictionary<byte, Action<NetMessage>> _rpcProcessors = new Dictionary<byte, Action<NetMessage>>();

        /// <summary>
        /// Subscribe to an rpc
        /// </summary>
        /// <param name="rpcID">id of the rpc</param>
        /// <param name="rpcProcessor">action to process the rpc with</param>
        /// <param name="overwriteExisting">overwrite the existing processor if one exists.</param>
        /// <returns>Whether or not the rpc was subscribed to. Will return false if an existing rpc was attempted to be subscribed to, and overwriteexisting was set to false</returns>
        public bool SubscribeToRpc(byte rpcID, Action<NetMessage> rpcProcessor, bool overwriteExisting)
        {
            if (rpcProcessor == null)
                throw new ArgumentNullException("rpcProcessor", "the processor delegate cannot be null");
            if (overwriteExisting)
            {
                _rpcProcessors[rpcID] = rpcProcessor;
                return true;
            }
            
            Action<NetMessage> checkExist;
            if (_rpcProcessors.TryGetValue(rpcID, out checkExist))
            {
                return false;
            }
                
            _rpcProcessors.Add(rpcID, rpcProcessor);
            return true;
        }
        
        internal void CallRpc(byte rpcID, NetMessage message)
        {
            Action<NetMessage> processor;
            if (_rpcProcessors.TryGetValue(rpcID, out processor))
            {
                if (processor != null)
                    processor(message);
                else
                {
                    Debug.LogWarning("RPC processor for {0} was null. Automatically cleaning up. Please be sure to clean up after yourself in the future.", rpcID);
                    _rpcProcessors.Remove(rpcID);
                }
            }
            else if (Manager.ConcernedAboutUnhandledRpcs)
            {
                 Debug.LogWarning("NetworkedSceneView on {0}: unhandled RPC {1}", NetworkID, rpcID);
            }
        }

        #endregion

        /// <summary>
        /// Send an rpc to the server
        /// </summary>
        /// <param name="rpcID"></param>
        /// <param name="args"></param>
        public void Rpc(byte rpcID, params object[] args)
        {
            var size = 0;
            foreach (var arg in args)
            {
                if (arg == null)
                    throw new NullReferenceException("Cannot serialize null value");

                size += _net.Serializer.SizeOf(arg);
            }

            var msg = StartMessage(rpcID, RpcMode.ServerOrdered, size);
            foreach (var arg in args)
            {
                _net.Serializer.Serialize(arg, msg);
            }
            _net.SendSceneViewMessage(msg, ReliabilityMode.Ordered);
        }

        NetMessage StartMessage(byte rpcId, RpcMode mode, int size)
        {
            var msg = _net.Room.GetMessage(size + 5);

            msg.Write(RpcUtils.GetHeader(mode, MsgType.Internal));
            msg.Write(RandPRpcs.SceneObjectRpc);
            msg.Write(NetworkID);
            msg.Write(rpcId);
            return msg;
        }

        /// <summary>
        /// serialize this into a string
        /// </summary>
        /// <returns></returns>
        public string Serialize()
        {
            var sb = new StringBuilder();
            sb.AppendLine("-NetworkedSceneObject-");
            sb.Append("id: ").Append(NetworkID).AppendLine(";");

            return sb.ToString();
        }

        #region IRpcProvider
        /// <summary>
        /// Subscribe to the specified rpc
        /// </summary>
        /// <param name="rpcId"></param>
        /// <param name="rpc">action to process the rpc with</param>
        /// <returns></returns>
        public bool SubscribeToRpc(byte rpcId, Action<NetMessage> rpc)
        {
            return SubscribeToRpc(rpcId, rpc, true);
        }

        /// <summary>
        /// Unsubscribe from an rpc
        /// </summary>
        /// <param name="rpcId"></param>
        public void UnsubscribeRpc(byte rpcId)
        {
            _rpcProcessors.Remove(rpcId);
        }

        /// <summary>
        /// subscribe all of the methods marked with RpcAttribute on the specified object
        /// </summary>
        /// <param name="obj"></param>
        public void SubscribeRpcsOnObject(object obj)
        {
            RpcSubscriber.SubscribeObject<RpcAttribute>(this, obj, _net.Serializer, Debug.Logger);
        }

        /// <summary>
        /// Clear all Rpc Subscriptions
        /// </summary>
        public void ClearSubscriptions()
        {
            _rpcProcessors.Clear();
        }
        #endregion

        #region IProxySingle<ISceneViewProxy>
        private ISceneViewProxy _proxyObject;
        /// <summary>
        /// the value set from Proxy(IServerProxy proxy)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Proxy<T>()
        {
            return (T)_proxyObject;
        }
        /// <summary>
        /// set the proxy object to use when returning Proxy`T()
        /// </summary>
        /// <param name="proxy"></param>
        public void Proxy(ISceneViewProxy proxy)
        {
            _proxyObject = proxy;
            if (_proxyObject != null)
                proxy.View = this;
        }
        #endregion
    }
}