﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PNet;
using PNet.Structs;

namespace PNetC
{
    public class NetworkViewManager
    {
        public Client Client { get; private set; }
        /// <summary>
        /// For rpcs that are not handled by a subscribed method, either allow or disallow them to continue forwarding
        /// </summary>
        public bool ConcernedAboutUnhandledRpcs { get; set; }

        public delegate void ViewInstantiate(NetworkView instance, Vector3F position, Vector3F rotation);

        readonly Dictionary<ushort, NetworkView> _networkViews = new Dictionary<ushort, NetworkView>();

        internal NetworkViewManager(Client client)
        {
            Client = client;
            ConcernedAboutUnhandledRpcs = true;
        }

        public NetworkView Get(NetworkViewId id)
        {
            NetworkView view;
            _networkViews.TryGetValue(id.Id, out view);
            return view;
        }

        internal void Instantiate(ushort newId, ushort owner, string resource, Vector3F position, Vector3F rotation)
        {
            var view = new NetworkView(new NetworkViewId(newId), this, resource);
            view.OwnerId = owner;
            NetworkView existing;
            if (_networkViews.TryGetValue(newId, out existing))
            {
                if (existing != null)
                    existing.Destroying(255);
                _networkViews.Remove(newId);
                Debug.LogError("Tried to instantiate {0} with id {1}, but it already exists as {2}", resource, newId, _networkViews[newId].Resource);
            }
            
            _networkViews.Add(newId, view);

            if (ViewInstantiated != null)
            {
                try
                {
                    ViewInstantiated(view, position, rotation);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }

        /// <summary>
        /// when a network view is instantiated. the Vector3F are position and rotation, in that order
        /// </summary>
        public event ViewInstantiate ViewInstantiated;

        internal void CallRpc(NetMessage msg, SubMsgType sub)
        {
            if (msg.RemainingBits < 24)
            {
                Debug.LogWarning("Attempted to call an rpc on a network view, but there weren't enough bits remaining to do it.");
                return;
            }
            var id = msg.ReadUInt16();
            var comp = msg.ReadByte();
            var rpc = msg.ReadByte();

            NetworkView view;
            if (_networkViews.TryGetValue(id, out view))
                view.IncomingRpc(comp, rpc, msg, sub);
            else
            {
                Debug.LogWarning("Could not find view {0} to call c {1} rpc {2}", id, comp, rpc);
            }
        }

        internal void Stream(NetMessage msg)
        {
            if (msg.RemainingBits < 16)
            {
                Debug.LogWarning("Attempted to read stream for an rpc but there weren't enough bits to do it");
                return;
            }
            var id = msg.ReadUInt16();
            NetworkView view;
            if (_networkViews.TryGetValue(id, out view))
                view.IncomingStream(msg);
            else
            {
                Debug.LogWarning("Could not find view {0} to stream to", id);
            }
        }

        internal void Destroy(NetMessage msg)
        {
            if (msg.RemainingBits < 16)
            {
                Debug.LogError("Malformed destroy");
            }
            
            var id = msg.ReadUInt16();
            byte reason = 0;
            if (msg.RemainingBits >= 8)
                reason = msg.ReadByte();
            
            NetworkView view;
            if (_networkViews.TryGetValue(id, out view) && view != null)
            {
                Debug.Log("Destroying {0} because {1}", id, reason);
                view.Destroying(reason);
                _networkViews.Remove(id);
            }
            else
            {
                Debug.LogError("Could not find {0} to destroy", id);
            }
        }

        internal void Hide(NetMessage msg)
        {
            if (msg.RemainingBits < 16)
            {
                Debug.LogError("Malformed hide");
            }

            var id = msg.ReadUInt16();

            NetworkView view;
            if (_networkViews.TryGetValue(id, out view) && view != null)
            {
                Debug.Log("Hiding {0} because", id);
                view.Hiding();
                _networkViews.Remove(id);
            }
            else
            {
                Debug.LogError("Could not find {0} to destroy", id);
            }
        }

        public void DestroyAll()
        {
            foreach (var kvp in _networkViews)
            {
                kvp.Value.Destroying(0);
            }
            _networkViews.Clear();
        }
    }
}