using System;
using PNet;

namespace PNetC
{
    public abstract class AContinuation
    {
        internal readonly byte ComponentId;
        internal readonly byte RpcId;

        //make this class not extensible nor instancable outside of the dll
        private AContinuation() { }

        internal AContinuation(byte compId, byte rpcId)
        {
            ComponentId = compId;
            RpcId = rpcId;
        }

        internal void RunError(NetMessage msg)
        {
            try
            {
                OnError(msg.ReadString());
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        internal void RunSuccess(NetMessage msg)
        {
            try
            {
                OnSuccess(msg);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
        protected abstract void OnError(string msg);
        protected abstract void OnSuccess(NetMessage msg);
    }

    /// <summary>
    /// Represents a set of actions that will run when the server completes the called rpc
    /// </summary>
    /// <typeparam name="T">The return type from the server. Must have defined serialize/deserialize information on the server/client</typeparam>
    public class Continuation<T> : AContinuation
    {
        private readonly Func<NetMessage, T> _deserialize;
        private Action<T> _complete;
        private Action<string> _error;
        private Action<T> _success;

        internal Continuation(byte compId, byte rpcId, Func<NetMessage, T> deserialize)
            : base(compId, rpcId)
        {
            _deserialize = deserialize;
        }

        public Continuation<T> Complete(Action<T> action)
        {
            _complete = action;
            return this;
        }

        public Continuation<T> Success(Action<T> action)
        {
            _success = action;
            return this;
        }

        public Continuation<T> Error(Action<string> action)
        {
            _error = action;
            return this;
        }

        protected override void OnError(string msg)
        {
            _error.Raise(msg);
            _complete.Raise(default(T));
        }

        protected override void OnSuccess(NetMessage msg)
        {
            var val = _deserialize(msg);
            _success.Raise(val);
            _complete.Raise(val);
        }
    }

    public class Continuation : Continuation<NetMessage>
    {

        internal Continuation(byte compId, byte rpcId) : base(compId, rpcId, message => message)
        {
        }
    }
}