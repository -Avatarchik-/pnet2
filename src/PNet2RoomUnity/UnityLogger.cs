﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PNet;
using UnityEngine;

namespace PNet2RoomUnity
{
    class UnityLogger : ILogger
    {
        public void Info(string info, params object[] args)
        {
            Debug.Log(string.Format(info, args));
        }

        public void Warning(string info, params object[] args)
        {
            Debug.LogWarning(string.Format(info, args));
        }

        public void Error(string info, params object[] args)
        {
            Debug.LogError(string.Format(info, args));
        }

        public void Exception(Exception exception, string info, params object[] args)
        {
            Debug.LogError(string.Format(info, args) + exception);
        }
    }
}
