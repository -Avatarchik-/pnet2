﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PNet;
using PNetC;
using PNetR;
using PNetS;
using Room = PNetR.Room;
using Server = PNetS.Server;

namespace IntegrationTests
{
    [TestClass]
    public class SetupTest
    {
        private Server _dispatcher;
        private TestRoom _room;
        private TestClient _client;

        private PNetS.Room _dRoom;
        private PNetS.Player _dClient;
        private PNetR.Player _rClient;
        
        private static TestContext _context;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _context = context;
            Integration.SetupLoggers();
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            
        }

        [TestInitialize]
        public void TestSetup()
        {
            _dispatcher = Integration.GetReadyServer();
            _room = Integration.GetConnectedRoom(_dispatcher);
            _client = Integration.GetConnectedClient(_dispatcher);

            Integration.WaitUntil(() => _dispatcher.GetPlayer(_client.Client.PlayerId) != PNetS.Player.ServerPlayer);
            _dClient = _dispatcher.GetPlayer(_client.Client.PlayerId);
            _dRoom = _dispatcher.GetRoom(_room.Room.RoomId);
            _rClient = _room.Room.GetPlayer(_client.Client.PlayerId);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            Console.WriteLine("------- cleanup -------");
            _dispatcher.Shutdown().GetAwaiter().GetResult();
            _room.Cleanup();
            _client.Cleanup();

            _dispatcher = null;
            _room = null;
            Thread.Sleep(100);
        }

        [TestMethod]
        public async Task EnsureConnected()
        {
            Assert.AreEqual(1, _dispatcher.RoomCount);
            Assert.AreEqual(ConnectionStatus.Connected, _room.Room.ServerStatus);

            await Integration.CheckAllLogs();
        }

        [TestMethod]
        public void MultipleRoomConnections()
        {
            var rooms = Integration.GetMultipleConnectedRooms();

            Assert.AreEqual(rooms.Length + 1, _dispatcher.RoomCount);

            
            //disconnect half of the rooms.
            for (int i = 0; i < rooms.Length; i++ )
            {
                if (i % 2 == 0) continue;

                rooms[i].Room.Shutdown();
            }

            for (int i = 0; i < rooms.Length; i++)
            {
                if (i % 2 == 0) continue;
                //wait up to one seconds to allow dc
                for (int j = 0; j < 1000; j++)
                {
                    if (ConnectionStatus.Disconnected == rooms[i].Room.ServerStatus)
                        break;
                    Thread.Sleep(1);
                }

                Assert.AreEqual(ConnectionStatus.Disconnected, rooms[i].Room.ServerStatus);
            }

            Integration.WaitUntil(() => rooms.Length/2 + 1 == _dispatcher.RoomCount);

            rooms.Cleanup();
        }

        [TestMethod]
        public async Task DisconnectionTest()
        {
            var cl2 = Integration.GetConnectedClient(_dispatcher);
            var dp1 = _dispatcher.GetPlayer(_client.Client.PlayerId);
            var dp2 = _dispatcher.GetPlayer(cl2.Client.PlayerId);
            var dr = _dispatcher.GetRoom(_room.Room.RoomId);
            dp1.ChangeRoom(dr);
            dp2.ChangeRoom(dr);
            
            Integration.WaitUntil(() => _client.Client.Room.RoomId == _room.Room.RoomId);
            Integration.WaitUntil(() => _room.Room.GetPlayer(_client.Client.PlayerId) != null);
            Integration.WaitUntil(() => cl2.Client.Room.RoomId == _room.Room.RoomId);
            Integration.WaitUntil(() => _room.Room.GetPlayer(cl2.Client.PlayerId) != null);

            var c1Called = false;
            var c2Called = false;
            _client.Client.Server.SubscribeToRpc(3, message =>
            {
                var rel = message.ReadString();
                Assert.AreEqual("hello", rel);
                c1Called = true;
            });

            cl2.Client.Server.SubscribeToRpc(3, message =>
            {
                var rel = message.ReadString();
                Assert.AreEqual("hello", rel);
                c2Called = true;
            });

            cl2.Client.Shutdown();
            Integration.WaitUntil(() => cl2.Client.Server.Status == ConnectionStatus.Disconnected, 4);

            dp1.PlayerRpc(3, "hello");

            Integration.WaitUntil(() => c1Called);
            Assert.IsFalse(c2Called);

            await Integration.CheckAllLogs();
        }

        [TestMethod]
#if !DEBUG
        [ExpectedException(typeof(AssertFailedException))]
#endif
        public void DispatchOnlyDisconnectTest()
        {
            _client.DispatchPlayer.NetUserData = new IntSerializer(357);

            _room.Room.ConstructNetData += () => new IntSerializer();

            Integration.SwitchRooms(_client, _room);

            _client.Client.Room.SubscribeToRpc(3, message => Assert.Fail("Should not have called the rpc on _client"));

            Console.WriteLine("Disconnecting unequally....");
            //disconnect client 1 from only the server to screw up ids on the room
            _client.Client.TestServerOnlyDisconnect();

            Integration.WaitUntil(() => _client.Client.Server.Status == ConnectionStatus.Disconnected);
            
            //cl2 should have the id _client just had, which will screw up the room that doesn't know about the disconnected player on the dispatcher
            var cl2 = Integration.GetConnectedClient(_dispatcher);
            if (cl2.Client.PlayerId != 1)
                Assert.Inconclusive("Expected the new player to get id 1, but got {0}. Does TestServerOnlyDisconnect work properly?", cl2.Client.PlayerId);
            cl2.DispatchPlayer.NetUserData = new IntSerializer(951);

            Assert.IsNotNull(_room.Room.GetPlayer(1));

            Integration.SwitchRooms(cl2, _room);

            var c2Called = false;
            cl2.Client.Room.SubscribeToRpc(3, message =>
            {
                var rel = message.ReadString();
                Assert.AreEqual("hello", rel);
                c2Called = true;
            });

            Console.WriteLine("Calling rpc to {0}", cl2.RoomPlayer);
            cl2.RoomPlayer.Rpc(3, "hello");
            Integration.WaitUntil(() => c2Called);
        }

        [TestMethod()]
        public void LoadDelayConnectTest()
        {
            _client.LoadDelay = 5000;
            var dp1 = _dispatcher.GetPlayer(_client.Client.PlayerId);
            var dr = _dispatcher.GetRoom(_room.Room.RoomId);
            dp1.ChangeRoom(dr);

            Integration.WaitUntil(() => _client.Client.Room.RoomId == _room.Room.RoomId, 6);
            Integration.WaitUntil(() => _room.Room.GetPlayer(_client.Client.PlayerId) != null);
        }

        [TestMethod]
        public async Task RoomSwitchTest()
        {
            var r1 = Integration.GetConnectedRoom(_dispatcher, 14101);

            await Integration.SwitchRoomsAsync(_client, r1);
            await Integration.SwitchRoomsAsync(_client, _room);

            await Integration.CheckAllLogs();
        }

        [TestMethod]
        public async Task DispatcherPlayerCountTests()
        {
            //remove the test player added by the test setup because it screws with the counts
            _client.Client.Shutdown();
            Integration.WaitUntil(() => _client.Client.Server.Status == ConnectionStatus.Disconnected);
            Integration.WaitUntil(() => _dispatcher.Players.Count(p => p != PNetS.Player.ServerPlayer) == 0);

            var clients = Enumerable.Range(0, 50)
                .AsParallel()
                .Select(i => Integration.GetConnectedClient(_dispatcher))
                .ToList();

            var disconnected = clients.Take(10).ToList();
            disconnected.AsParallel().ForAll(client => client.Client.Shutdown());
            
            clients = clients.Skip(10).ToList();

            var addSet1 = Enumerable.Range(0, 30).Select(i => Integration.GetConnectedClientAsync(_dispatcher)).ToList();

            var removeSet1 = addSet1.Skip(10).Take(10).Select(async task =>
            {
                var client = await task;
                client.Client.Shutdown();
                await Integration.WaitUntilAsync(() => client.Client.Server.Status == ConnectionStatus.Disconnected);
                return client;
            }).ToList();
            for (int i = addSet1.Count - 1; i >= 0; i--)
            {
                if (i > 19) continue;
                if (i < 10) continue;
                addSet1.RemoveAt(i);
            }

            clients.ForEach(client => Integration.WaitUntil(() => client.Client.Server.Status == ConnectionStatus.Connected));
            disconnected.ForEach(client => Integration.WaitUntil(() => client.Client.Server.Status == ConnectionStatus.Disconnected));

            const int timeout = 1000;

            //so we'll throw an error if whenall doesn't work fast enough
            var cts = new CancellationTokenSource(timeout);
            await Task.WhenAny(Task.WhenAll(addSet1), Task.Delay(timeout*2, cts.Token));

            cts = new CancellationTokenSource(timeout);
            await Task.WhenAny(Task.WhenAll(removeSet1), Task.Delay(timeout * 2, cts.Token));

            clients.AddRange(addSet1.Select(t => t.Result));
            disconnected.AddRange(removeSet1.Select(t => t.Result));


            clients.ForEach(
                client => Assert.AreEqual(client.Token, _dispatcher.GetPlayer(client.Client.PlayerId).UserData as string));

            disconnected.ForEach(client => Integration.WaitUntil(() => client.DispatchPlayer.Status == ConnectionStatus.Disconnected));

            //sort the orders so comparison in debugging is easy
            var realPlayers = _dispatcher.Players.Where(p => p != PNetS.Player.ServerPlayer).ToList().OrderBy(p => p.UserData as string).ToList();
            clients = clients.OrderBy(c => c.Token).ToList();
            var endCount = _dispatcher.PlayerCount;

            Assert.AreEqual(clients.Count, realPlayers.Count);
            Assert.AreEqual(endCount, realPlayers.Count);
        }

        [TestMethod]
        public async Task RoomPlayerCountTest()
        {
            var r1 = Integration.GetConnectedRoom(_dispatcher, 14101, maxPlayers: 50);
            var r2 = Integration.GetConnectedRoom(_dispatcher, 14102, maxPlayers:50);
            //remove the test player added by the test setup because it screws with the counts
            _client.Client.Shutdown();
            Integration.WaitUntil(() => _client.Client.Server.Status == ConnectionStatus.Disconnected);
            Integration.WaitUntil(() => _dispatcher.Players.Count(p => p != PNetS.Player.ServerPlayer) == 0);

            var clients = Enumerable.Range(0, 50)
                .AsParallel()
                .Select(i => Integration.GetConnectedClient(_dispatcher))
                .ToList();

            Assert.AreEqual(0, r1.DispatchRoom.PlayerCount);
            Assert.AreEqual(0, r1.Room.PlayerCount);
            Assert.AreEqual(0, r2.DispatchRoom.PlayerCount);
            Assert.AreEqual(0, r2.Room.PlayerCount);

            var rClients = clients.Where((c, i) => i%3 == 0).Select(async c =>
            {
                await Integration.SwitchRoomsAsync(c, r1);
                return c;
            }).ToList();

            var r2Clients = clients.Where((c, i) => i%3 == 1).Select(async c =>
            {
                await Integration.SwitchRoomsAsync(c, r1);
                await Integration.SwitchRoomsAsync(c, r2);
                return c;
            }).ToList();

            var r3Clients = clients.Where((c, i) => i%3 == 2).Select(async c =>
            {
                await Integration.SwitchRoomsAsync(c, r1);
                c.Client.Shutdown();
                return c;
            }).ToList();

            const int timeout = 1000;

            //so we'll throw an error if whenall doesn't work fast enough
            var cts = new CancellationTokenSource(timeout);
            await Task.WhenAny(Task.WhenAll(rClients), Task.Delay(timeout*2, cts.Token));

            cts = new CancellationTokenSource(timeout);
            await Task.WhenAny(Task.WhenAll(r2Clients), Task.Delay(timeout * 2, cts.Token));

            cts = new CancellationTokenSource(timeout);
            await Task.WhenAny(Task.WhenAll(r3Clients), Task.Delay(timeout * 2, cts.Token));

            await Integration.CheckAllLogs();

            Assert.AreEqual(0, _room.Room.PlayerCount);

            //the old rooms will still have the players a bit after SwitchRoomsAsync returns
            //so we'll wait for the players instead of just assuming they're already at the value
            await Integration.WaitUntilAsync(() => r1.Room.Players.Count(p => p != null && p != PNetR.Player.Server) ==
                                             rClients.Count, 10);

            await Integration.WaitUntilAsync(() => r2.Room.Players.Count(p => p != null && p != PNetR.Player.Server) ==
                                             r2Clients.Count, 10);

            Assert.AreEqual(rClients.Count, r1.Room.PlayerCount);
            Assert.AreEqual(r2Clients.Count, r2.Room.PlayerCount);

            //because the shutdowns don't happen instantly, so we need to keep checking for a bit
            await
                Integration.WaitUntilAsync(
                    () =>
                        _dispatcher.Players.Count(p => p != null && p.CurrentRoomGuid == r1.Room.RoomId) ==
                        rClients.Count);
            await
                Integration.WaitUntilAsync(
                    () =>
                        _dispatcher.Players.Count(p => p != null && p.CurrentRoomGuid == r2.Room.RoomId) == 
                        r2Clients.Count);

            Assert.AreEqual(rClients.Count, r1.DispatchRoom.PlayerCount);
            Assert.AreEqual(r2Clients.Count, r2.DispatchRoom.PlayerCount);
        }
    }
}
