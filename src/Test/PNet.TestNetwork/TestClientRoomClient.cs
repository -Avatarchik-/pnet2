﻿using System;
using System.Collections.Generic;
using PNetC;

namespace PNet.TestNetwork
{
    public class TestClientRoomClient : ARoomClient
    {
        internal PNetR.Player RoomPlayer;
        public TestRoomServer Server { get; set; }

        readonly Queue<NetMessage> _messages = new Queue<NetMessage>();

        protected override void Start()
        {
        }

        protected override void SwitchRoom()
        {
            if (Client.Room != null && Client.Room.Status != ConnectionStatus.Disconnected)
            {
#if DEBUG
                Debug.Log("begin room switch waiting for disconnect");
#endif
                SwitchState = waitForSwitchState.WaitForDc;
                Disconnect(PtoDMsgs.RoomSwitch);
            }
            else
            {
#if DEBUG
                Debug.Log("begin room switch waiting for room switch");
#endif
                WaitForRoomSwitch();
            }
        }

        protected override void ImplWaitForReconnect()
        {
            Server.Connect(this, SwitchToken);
        }

        protected override void ReadQueue()
        {
            NetMessage[] messages;
            lock (_messages)
            {
                messages = _messages.ToArray();
                _messages.Clear();
            }
            foreach (var msg in messages)
                ConsumeData(msg);
        }

        protected override void Disconnect(string reason)
        {
            Server.Internal_Disconnect(this, reason);
        }

        protected override NetMessage GetMessage(int size)
        {
            return NetMessage.GetMessage(size);
        }

        protected override void SendSceneViewMessage(NetMessage msg, ReliabilityMode mode)
        {
            SendMessage(msg, true);
        }

        protected override void SendViewMessage(NetMessage msg, ReliabilityMode mode)
        {
            SendMessage(msg, true);
        }

        protected override void SendStaticMessage(NetMessage msg, ReliabilityMode mode)
        {
            SendMessage(msg, true);
        }

        private void SendMessage(NetMessage msg, bool recycle)
        {
            var send = NetMessage.GetMessage(msg.LengthBytes);
            msg.Clone(send);
            Server.ReceiveMessage(this, send);
            if (recycle)
                NetMessage.RecycleMessage(msg);
        }

        internal void ReceiveMessage(NetMessage msg)
        {
            lock (_messages)
                _messages.Enqueue(msg);
        }

        internal void Disconnected(string reason)
        {
            Debug.Log("Disconnected: {0}", reason);
            if (SwitchState == waitForSwitchState.WaitForDc)
            {
                //fully disconnected. now we can tell users that they should switch rooms.
                WaitForRoomSwitch();
            }
        }

        internal void AllowConnect(PNetR.Player player, Guid guid)
        {
            RoomPlayer = player;

            ConnectedToRoom(guid);
        }
    }
}
