﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PNet;

namespace PNet2UnitTests
{
    [TestClass]
    public class NetMessageTests
    {
        [TestMethod]
        public void Log2GetMessageTest()
        {
            var msg = NetMessage.GetMessage(0);
            Assert.AreEqual(2, msg.Data.Length);
            NetMessage.RecycleMessage(msg);

            msg = NetMessage.GetMessage(2);
            Assert.AreEqual(2, msg.Data.Length);
            NetMessage.RecycleMessage(msg);

            msg = NetMessage.GetMessage(3);
            Assert.AreEqual(4, msg.Data.Length);
            NetMessage.RecycleMessage(msg);

            msg = NetMessage.GetMessage(4);
            Assert.AreEqual(4, msg.Data.Length);
            NetMessage.RecycleMessage(msg);

            msg = NetMessage.GetMessage(64);
            Assert.AreEqual(64, msg.Data.Length);
            NetMessage.RecycleMessage(msg);
        }
    }
}
