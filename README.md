![pnetlogo150_by_faikie-d7rfu8s.png](https://bitbucket.org/repo/EApo9z/images/454951087-pnetlogo150_by_faikie-d7rfu8s.png)

Networking middleware for Unity using Lidgren, with a custom dedicated server.


Directions
----------
This library uses Lidgren(client/server), with some patches to Lidgren to work with Unity. Lidgren is referenced from my own fork of the lidgren github repository as a submodule. I will attempt to keep it up to date.

The PNet.dll, PNetC.dll, PNetU.dll, and Lidgren.Network.dll should be copied to a Plugins folder in unity. PNet, PNetS, SlimMath, and Lidgren.Network should be referenced in the server.
When writing for Unity, reference the PNetU namespace, as it is Unity glue code to PnetC.

Similarity to unity's (old) networking
-----
Networking works similarly to unity's networking, with methods on components being marked with an RpcAttribute.

Some differences in PNet:

* RPC's can be verified by the server. The NetworkInfo object that is passed into the delegate for that rpc has a variable called continueForwarding. If set to false, the rpc will not go to other clients if the rpcmode was Other or All.
  
Some differences from the Unity Network class on the client  

* The RpcAttribute requires you specify the ID of the rpc, which is a byte. It is recommended you set up a shared project on the client and server that has these IDs defined as constants, so that you actually have a name for the byte values.  
* As rpc's only get passed to the same NetworkView as them, you can overlap RPC IDs, so long as you aren't overlapping components listening to said RPCs  
* Network instantiated objects need to exist in the Resources folder on the client.  
* NetworkViews can have custom tick rates, completely independent of one another.   
* Only the server can call NetworkInstantiate, for obvious security reasons.  Set up your own RPCs to request a spawn if clients should be able to do so  
* Scripts with Rpc marks need to be attached to the prefab on the client before it is instantiated, as that is when the attributes are found and subscribed. If you want to attach a component  afterward, you can use the NetworkView.SubscribeToRPC method.  

Changes from original PNet
---
Functionality

* The Server has been divided into two parts: The dispatcher, and room servers. The dispatcher acts as a 'master server' that all clients and all rooms are connected to. The individual room servers are basically smaller servers that clients are told to connect to. They can become effectively instances of each other, so that you can load balance players between multiple room servers for the same scene.
* Room servers do not have a 'game engine'. The room servers are more like what the PNetC library is. The game engine from the original PNet has been separated out into this project: https://github.com/jbruening/Nent, which you can use. However, because the room servers target 3.5, it is entirely possible to use a headless unity instance as the room server, which means you can have identical physics on the server as on the client.
* As the game engine has been removed from the server, and there is no longer a concept of components, subscription to rpc-marked classes is done through the  I(Info)RpcProvider interfaces. You'll usually see this as SubscribeRpcsOnObject, in both the client and room networkview classes.

Additions

* Rpc serialization/calling now has built-in serialization for the basic .net types (string, byte, int, etc). You can also add serializer/deserializer functions for your own types through the SerializationManager objects that are properties of Client and Room.  The main bonus this provides is your rpc-marked methods no longer have to perform manual deserialization.

* Proxy objects can be kept track of, and used to call rpcs like a standard method call. Use of proxies can be seen in the PlayerAndRoomNetworkViewRpcTests.ProxyTest test method.  It is recommended, however, that you use something like t4 templates, post-compilation weaving, or Castle.Windsor's dynamic proxy generation in order to create your proxy types.

License: 
---
PNet is distributed under the MIT license as following:

The MIT License (MIT) Copyright (c) 2012 Justin Bruening

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.